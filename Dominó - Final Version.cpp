#include <iostream>
#include <ctime>
#include <cstdlib>
#include <locale.h>
#include <windows.h>
#include <ctype.h>
#include <fstream>

using namespace std;

char jogador1[51] = "Jogador 1", jogador2[51] = "Jogador 2", nome_arquivo[60], conteudo_arquivo[108];
int pontos, pontuacao1, pontuacao2;
int tabuleiro[56], pecas[56], mao1[42], mao2[42], puxa[28];
int semente, aleatorio, tempo_com;
int indice_aleatorio, entrada_peca_jogada, entrada_parte_tabuleiro, contador, tamanho_tabuleiro, tamanho_puxa, tamanho_mao1, tamanho_mao2, tamanho_arquivo;
int parte_superior, parte_inferior, somatorio_peca1, somatorio_peca2, soma_maior, soma_pecas1, soma_pecas2, quantidade_caracteres;
int indice_tabuleiro, indice_pecas, indice_mao, indice_mao1, indice_mao2, indice_puxa, indice_arquivo, indice_nomes, cima, baixo;
int maior1_superior, maior1_inferior, maior2_superior, maior2_inferior;
int localizacao_peca_maior1, localizacao_peca_maior2, entrada_modo, temp;
bool ganhou1, ganhou2, jogada_invalida, vez, colocarAcima, colocarAbaixo, puxou, primeira_jogada_e_partida, temCarroca1, temCarroca2;
bool bateu_la_e_lo, bateu_carroca, jogoTravado, resultado, repetir_menu_inicial, humano_contra_computador, continuar_jogo, jogoCarregado;
char entrada_menu_opcoes, entrada_menu_inicial_opcoes, entrada_termino_partida;

/**
    Recebe um inteiro e retorna o caractere correspondente da tabela ASCII.
*/
char ascii(int valor){
    return (char) valor;
}

/**
    Decide se o programa vai dormir ou pausar.
    Caso seja a vez do computador, ele faz o programa dormir. Sen�o, o programa pausa.
*/
void PausarOuDormir(){
    if (humano_contra_computador && !vez){
        Sleep(tempo_com);
    }else if (!humano_contra_computador){
        system("pause");
    }
}

/**
    Altera o idioma para Portugu�s.
*/
void naoUsareiAscii(){
    setlocale(LC_ALL, "Portuguese");
}

/**
    Altera o idioma para o padr�o para evitar erros no uso da tabela ASCII.
*/
void usareiAscii(){
    setlocale(LC_ALL, "C");
}

/**
    Testa se a pe�a passada como par�metro faz l� e l� com o tabuleiro atual havendo
    diferen�a para o jogo em qual lado a pe�a ser� colocada.
*/
bool laELoFazDiferencaQualLado(int superior, int inferior){
    resultado =
    (superior == tabuleiro[tamanho_tabuleiro-1] && inferior == tabuleiro[0] ||
    superior == tabuleiro[0] && inferior == tabuleiro[tamanho_tabuleiro-1])
    && superior != inferior;

    return resultado;
}

/**
    Testa se a pe�a passada como par�metro faz l� e l� com o tabuleiro atual n�o havendo
    diferen�a para o jogo em qual lado a pe�a ser� colocada.
*/
bool laELoNaoFazDiferencaQualLado(int superior, int inferior){
    resultado =
    superior == tabuleiro[0] && superior == tabuleiro[tamanho_tabuleiro-1] ||
    inferior == tabuleiro[0] && inferior == tabuleiro[tamanho_tabuleiro-1];

    return resultado;
}

/**
    Testa se o vetor m�o passado como par�metro possui alguma pe�a jog�vel no tabuleiro atual.
*/
bool maoContemPecaJogavel(int *mao){
    indice_mao = 0;

    while(mao[indice_mao] != -1 && indice_mao < 42){
        if (mao[indice_mao] == tabuleiro[0] || mao[indice_mao] == tabuleiro[tamanho_tabuleiro-1] ||
            mao[indice_mao+1] == tabuleiro[0] || mao[indice_mao+1] == tabuleiro[tamanho_tabuleiro-1]){
            return true;
        }
        indice_mao += 2;
    }

    return false;
}

/**
    Testa se a pe�a passada como par�metro � jog�vel no tabuleiro atual.
*/
bool pecaJogavel(int superior, int inferior){
    resultado =
    superior == tabuleiro[0] || superior == tabuleiro[tamanho_tabuleiro-1] ||
    inferior == tabuleiro[0] || inferior == tabuleiro[tamanho_tabuleiro-1];

    return resultado;
}

/**
    Retorna o indice de uma pe�a jog�vel da m�o 2.
    Retorna -1 caso n�o haja pe�a jog�vel na m�o 2.
*/
int escolhePeca(){

    // Se o tabuleiro estiver vazio, escolhe uma pe�a aleatoriamente:
    if (tabuleiro[0] == -1){
        aleatorio = rand() % 7;
        return aleatorio;
    }

    // Escolhe a primeira pe�a jog�vel que achar:
    indice_mao2 = 0;
    while(mao2[indice_mao2] != -1 && indice_mao2 < 42){
        if (pecaJogavel(mao2[indice_mao2], mao2[indice_mao2+1])){
            return indice_mao2 / 2;
        }
        indice_mao2 += 2;
    }

    return -1;
}

/**
    Recebe uma pe�a da m�o 2 que pode ser jogada l� e l� no tabuleiro atual e determina
    em qual lado o computador colocar� a pe�a.
*/
void escolheLado(){
    aleatorio = rand() % 2;

    if (aleatorio == 0){
        colocarAcima = true;
    }else{
        colocarAbaixo = true;
    }
}

/**
    Imprime a quantidade de espa�os passada como par�metro.
*/
void imprimeEspacos(int quantidade){
    for (int i = 0; i < quantidade; i++){
        cout << " ";
    }
}

/**
    Imprime uma linha.
*/
void imprimeLinha(){
    usareiAscii();
   for (contador = 0; contador < 80; contador++){
    cout << ascii(196);
   }
   naoUsareiAscii();
}

/**
    Imprime uma arte ASCII que representa o nome do jogo.
*/
void imprimeArteAscii(){
    imprimeLinha();

    int tempo1 = 100;
    int tempo2 = 2100;

    cout << endl << endl << endl;

    cout << "      _/      _/                                _/                            " << endl << "\a";
    Sleep(tempo1);
    cout << "     _/_/  _/_/    _/_/    _/_/_/      _/_/_/        _/_/    _/    _/  _/  _/_/" << endl << "\a";
    Sleep(tempo1);
    cout << "    _/  _/  _/  _/    _/  _/    _/  _/_/      _/  _/_/_/_/  _/    _/  _/_/" << endl << "\a";
    Sleep(tempo1);
    cout << "   _/      _/  _/    _/  _/    _/      _/_/  _/  _/        _/    _/  _/" << endl << "\a";
    Sleep(tempo1);
    cout << "  _/      _/    _/_/    _/    _/  _/_/_/    _/    _/_/_/    _/_/_/  _/ " << endl << "\a";
    Sleep(tempo1);

    cout << endl << endl;

    cout << "             _/_/_/                              _/               _/  " << endl << "\a";
    Sleep(tempo1);
    cout << "            _/    _/    _/_/    _/_/_/  _/_/        _/_/_/      _/_/  " << endl << "\a";
    Sleep(tempo1);
    cout << "           _/    _/  _/    _/  _/    _/    _/  _/  _/    _/  _/    _/ " << endl << "\a";
    Sleep(tempo1);
    cout << "          _/    _/  _/    _/  _/    _/    _/  _/  _/    _/  _/    _/  " << endl << "\a";
    Sleep(tempo1);
    cout << "         _/_/_/      _/_/    _/    _/    _/  _/  _/    _/    _/_/     " << endl << "\a";

    cout << endl << endl << endl;

    cout << "                --- Por Miguel de Oliveira e Miguel Angelo ---" << endl;

    imprimeLinha();

    Sleep(tempo2);
    system("cls");
}

/**
    Imprime o vetor m�o passado como par�metro.
*/
void imprimeMao(int *mao){

    usareiAscii();

    indice_mao = 0;
    contador = 1;
    char cima, baixo;

    while(mao[indice_mao] != -1 && indice_mao < 42){

        if (humano_contra_computador && !vez){
            cima = ascii(178);
            baixo = ascii(178);
        }else{
            cima = ascii(mao[indice_mao] + 48);
            baixo = ascii(mao[indice_mao+1] + 48);
        }

        imprimeEspacos(4);
        if (contador >= 10){
            imprimeEspacos(1);
        }
        cout << ascii(218) << ascii(196) << ascii(194) << ascii(196) << ascii(191) << endl;

        cout << contador << " " << ascii(26) << " " << ascii(179) << cima << ":" << baixo << ascii(179)  << endl;
        imprimeEspacos(4);
        if (contador >= 10){
            imprimeEspacos(1);
        }
        cout << ascii(192) << ascii(196) << ascii(193) << ascii(196) << ascii(217) << endl;

        indice_mao += 2;
        contador++;
    }

    naoUsareiAscii();
}

/**
    Imprime o vetor tabuleiro.
*/
void imprimeTabuleiro(){

    imprimeLinha();
    usareiAscii();

    indice_tabuleiro = 0;

    while(tabuleiro[indice_tabuleiro] != -1 && indice_tabuleiro < 56){

        cout << ascii(218) << ascii(196) << ascii(191) << endl;
        cout << ascii(179) << tabuleiro[indice_tabuleiro] << ascii(179) << endl;
        cout << ascii(195) << "=" << ascii(180);

        if (tabuleiro[indice_tabuleiro+2] != -1){
            cout << ascii(218) << ascii(196) << ascii(191) << endl;
        }else{
            cout << endl;
        }

        cout << ascii(179) << tabuleiro[indice_tabuleiro+1] << ascii(179);

        if (tabuleiro[indice_tabuleiro+2] != -1){
            cout << ascii(179) << tabuleiro[indice_tabuleiro+2] << ascii(179) << endl;
        }else{
            cout << endl;
        }

        cout << ascii(192) << ascii(196) << ascii(217);

        if (tabuleiro[indice_tabuleiro+2] != -1){
            cout << ascii(195) << "=" << ascii(180) << endl;
            imprimeEspacos(3);
            cout << ascii(179) << tabuleiro[indice_tabuleiro+3] << ascii(179) << endl;
            imprimeEspacos(3);
            cout << ascii(192) << ascii(196) << ascii(217) << endl;
        }

        indice_tabuleiro +=  4;
    }
    cout << endl;

    naoUsareiAscii();
    imprimeLinha();
}

/**
    Recebe o nome do jogador que iniciar� a primeira jogada da primeira partida e a localiza��o
    da maior pe�a em sua m�o.
    Imprime o nome do jogador, informando que ele iniciar� a partida e tamb�m imprime qual pe�a
    o jogador deve jogar.
*/
void imprimeJogadorDaPrimeiraPartida(char *jogador, int localizacao_peca_maior){
    if (humano_contra_computador && !vez){
        cout << "O computador jogar� primeiro devido � sua pe�a " << localizacao_peca_maior+1 << "." << endl;
    }else{
        cout << jogador << ", voc� jogar� primeiro, mas voc� deve colocar a pe�a " << localizacao_peca_maior+1 << ",";
        cout << " pois ela � a pe�a que fez voc� ser o primeiro." << endl;
    }
}

/**
    Recebe o nome de um jogador e imprime uma mensagem informando que � a vez dele.
*/
void imprimeJogadoDaVez(char *jogador){
    if (humano_contra_computador && !vez){
        cout << "� a vez do computador." << endl;
    }else{
        cout << jogador << ", � a sua vez." << endl;
    }
}

/**
    Imprime uma mensagem de vit�ria informando como o vencedor ganhou, quantos pontos ganhou e
    a atualiza��o do placar.
    Tamb�m soma os pontos ganhos.
*/
void imprimeMensagemVitoriaESomaPontos(char *vencedor){

    pontos = 0;

    if (jogoTravado){
        cout << "Jogo travado." << endl;
        cout << "Soma das pe�as: " << endl;
        cout << jogador1 << ": " << soma_pecas1 << endl;
        cout << jogador2 << ": " << soma_pecas2 << endl;

        if (humano_contra_computador && ganhou2){
            cout << "O computador ";
        }else{
            cout << "Parab�ns " << vencedor << ", voc� ";
        }

        cout << "ganhou por travamento e somou um ponto." << endl;
        pontos = 1;
    }
    else if (bateu_carroca && bateu_la_e_lo){
        if (humano_contra_computador && ganhou2){
            cout << "O computador ";
        }else{
            cout << "Parab�ns " << vencedor << ", voc� ";
        }

        cout << "ganhou somando um ponto da vit�ria mais cinco porque foi l� e l� com carro�a." << endl;
        pontos = 1 + 5;
    }
    else if (bateu_carroca){
        if (humano_contra_computador && ganhou2){
            cout << "O computador ";
        }else{
            cout << "Parab�ns " << vencedor << ", voc� ";
        }

        cout << "ganhou somando um ponto da vit�ria mais um ponto por bater com carro�a." << endl;
        pontos = 1 + 1;
    }
    else if (bateu_la_e_lo){
        if (humano_contra_computador && ganhou2){
            cout << "O computador ";
        }else{
            cout << "Parab�ns " << vencedor << ", voc� ";
        }

        cout << "ganhou somando um ponto da vit�ria mais dois por bater l� e l�." << endl;
        pontos = 1 + 2;
    }else{
        if (humano_contra_computador && ganhou2){
            cout << "O computador ";
        }else{
            cout << "Parab�ns " << vencedor << ", voc� ";
        }

        cout << "ganhou por batida simples somando um ponto. " << endl;
        pontos = 1;
    }

    if (ganhou1){
        pontuacao1 += pontos;
    }else{
        pontuacao2 += pontos;
    }

    imprimeLinha();

    cout << jogador1 << ": " << pontuacao1 << " ponto(s)." << endl;
    cout << jogador2 << ": " << pontuacao2 << " ponto(s). " << endl;

    imprimeLinha();

    cout << "\a";
}

/**
    Imprime uma mensagem de entrada inv�lida.
*/
void imprimeMensagemEntradaInvalida(){
    imprimeLinha();
    cout << "Entrada inv�lida." << endl;
}

/**
    Emite um som, muda a cor do prompt para vermelho, imprime uma mensagem p�s erro e pausa o programa.
*/
void jogadaInvalida(){
    cout << "\a";
    system("color 4");

    cout << "Tente novamente: " << endl;
    imprimeLinha();

    system("pause");

    if (vez){
        system("color 2");
    }else{
        system("color 6");
    }
}

/**
    Recebe do usu�rio o nome do arquivo de salvamento do jogo, colocando o nome do diret�rio
    "info" no in�cio e a extens�o ".dom" no fim.
*/
void entradaNomeArquivo(){

    cout << "Informe o nome do arquivo" << endl;

    // Corrige bug:
    cin.getline(nome_arquivo, 1);

    // Entrada definitiva:
    cin.getline(nome_arquivo, 51);


    // Determina o tamanho do nome do arquivo:

    tamanho_arquivo = 0;

    while(nome_arquivo[tamanho_arquivo] != '\0'){
        tamanho_arquivo++;
    }

    // Desocupa as 5 primeiras posi��es:

    for (indice_arquivo = tamanho_arquivo-1+5; indice_arquivo >= 5; indice_arquivo--){
        nome_arquivo[indice_arquivo] = nome_arquivo[indice_arquivo-5];
    }

    // Preenche as 5 primeiras posi��es:

    nome_arquivo[0] = 'i';
    nome_arquivo[1] = 'n';
    nome_arquivo[2] = 'f';
    nome_arquivo[3] = 'o';
    nome_arquivo[4] = '\\';

    // Coloca a extens�o .dom no final:

    indice_arquivo = tamanho_arquivo + 5;

    nome_arquivo[indice_arquivo] = '.';
    nome_arquivo[indice_arquivo+1] = 'd';
    nome_arquivo[indice_arquivo+2] = 'o';
    nome_arquivo[indice_arquivo+3] = 'm';
    nome_arquivo[indice_arquivo+4] = '\0';
}

/**
    Recebe do usu�rio o nome do arquivo de salvamento, cria um arquivo com este nome e
    extens�o ".dom" na pasta "info" e escreve as informa��es do jogo atual nele.
*/
void salvaJogo(){

    system("cls");

    entradaNomeArquivo();

    // In�cio da forma��o do conte�do do arquivo:

    // Coloca o nome do jogador 1 na primeira linha:

    indice_nomes = 0;
    indice_arquivo = 0;

    while(jogador1[indice_nomes] != '\0'){
        conteudo_arquivo[indice_arquivo] = jogador1[indice_nomes];
        indice_arquivo++;
        indice_nomes++;
    }

    conteudo_arquivo[indice_arquivo] = '\n';

    // Coloca o nome do jogador 2 na segunda linha:

    indice_nomes = 0;
    indice_arquivo++;

    while(jogador2[indice_nomes] != '\0'){
        conteudo_arquivo[indice_arquivo] = jogador2[indice_nomes];
        indice_arquivo++;
        indice_nomes++;
    }

    conteudo_arquivo[indice_arquivo] = '\n';

    // Coloca a pontua��o do jogador 1 na terceira linha:

    conteudo_arquivo[indice_arquivo+1] = ascii(pontuacao1 + 48);
    conteudo_arquivo[indice_arquivo+2] = '\n';

    // Coloca a pontua��o do jogador 2 na quarta linha:

    conteudo_arquivo[indice_arquivo+3] = ascii(pontuacao2 + 48);
    conteudo_arquivo[indice_arquivo+4] = '\n';

    // Coloca o �ltimo jogador que ganhou na quinta linha

    conteudo_arquivo[indice_arquivo+5] = ascii(ganhou1 + 48);
    conteudo_arquivo[indice_arquivo+6] = '\n';

    // Coloca o modo de jogo na �ltima linha:

    conteudo_arquivo[indice_arquivo+7] = ascii(humano_contra_computador + 48);
    conteudo_arquivo[indice_arquivo+8] = '\0';

    // Fim da forma��o do conte�do do arquivo:

    // Cria o diret�rio:
    // Vai ocorrer um erro caso o diret�rio j� exista, mas a mensagem ser� omitida.

    system("md info");
    system("cls");

    // Remove o atributo somente leitura dos arquivos da pasta info :
    system("attrib -r info\\*.*");

    // Cria��o do arquivo:

    ofstream arquivo(nome_arquivo);

    // Grava��o do conte�do no arquivo:

    arquivo << conteudo_arquivo;

    // Fechamento do fluxo:

    arquivo.close();

    // Atribui aos arquivos da pasta info o atributo somente leitura:
    system("attrib +r info\\*.*");
 }

/**
    Recebe do usu�rio o nome de um arquivo e carrega as informa��es presentes nele.
*/
void carregaJogo(){

    system("cls");

    entradaNomeArquivo();

    ifstream arquivo(nome_arquivo);

    conteudo_arquivo[0] = '\n';

    indice_arquivo = 0;

    while(arquivo.get(conteudo_arquivo[indice_arquivo])){
        indice_arquivo++;
    }

    if (conteudo_arquivo[0] != '\n'){

        // Carrega o nome do jogador 1:

        indice_nomes = 0;
        indice_arquivo = 0;

        while(conteudo_arquivo[indice_arquivo] != '\n'){
            jogador1[indice_nomes] = conteudo_arquivo[indice_arquivo];
            indice_nomes++;
            indice_arquivo++;
        }

        jogador1[indice_nomes] = '\0';

        // Carrega o nome do jogador 2:

        indice_nomes = 0;
        indice_arquivo++;

        while(conteudo_arquivo[indice_arquivo] != '\n'){
            jogador2[indice_nomes] = conteudo_arquivo[indice_arquivo];
            indice_nomes++;
            indice_arquivo++;
        }

        jogador2[indice_nomes] = '\0';

        // Carrega a pontua��o do jogador 1:

        indice_arquivo++;

        pontuacao1 = (int) ( conteudo_arquivo[indice_arquivo] - 48 );

        // Carrega a pontua��o do jogador 2:

        indice_arquivo += 2;

        pontuacao2 = (int) ( conteudo_arquivo[indice_arquivo] - 48 );

        // Carrega o jogador a iniciar a pr�xima partida:

        indice_arquivo += 2;

        vez = (int) ( conteudo_arquivo[indice_arquivo] - 48 );

        // Carrega o modo do jogo:

        indice_arquivo += 2;

        humano_contra_computador = (int) ( conteudo_arquivo[indice_arquivo] - 48 );

        // Determina que j� n�o a primeira partida e primeira jogada:
        primeira_jogada_e_partida = false;

        // Declara que o jogo foi carregado com sucesso:

        jogoCarregado = true;

        cout << "Jogo carregado com sucesso." << endl;
        Sleep(1350);
        cout << "Nome do jogador 1: " << jogador1 << endl;
        Sleep(1350);
        cout << "Nome do jogador 2: " << jogador2 << endl;
        Sleep(1350);
        cout << "Pontua��o de " << jogador1 << ": " << pontuacao1 << endl;
        Sleep(1350);
        cout << "Pontua��o de " << jogador2 << ": " << pontuacao2 << endl;

        Sleep(1350);

        if (vez){
            cout << jogador1 << " iniciar� a pr�xima partida." << endl;
        }else{
            cout << jogador2 << " iniciar� a pr�xima partida." << endl;
        }

        Sleep(1350);

        if (humano_contra_computador){
            cout << "O jogo est� no modo humano contra computador." << endl;
        }else{
            cout << "O jogo est� no modo humano contra humano." << endl;
        }

        Sleep(1350);

    }else{
        imprimeLinha();
        cout << "O arquivo informado n�o existe." << endl;
        jogada_invalida = true;
        jogadaInvalida();
    }

}

/**
    Executa a��es necess�rias antes do in�cio da partida.
    Embaralha o vetor de pe�as.
    Distribui as pe�as aleatoriamente.
    Preenche os indices ainda n�o utilizados com o valor -1.
    Determina qual jogador iniciar� a partida.
*/
void determinacoesIniciais(){

  // Embaralhamento do vetor de pe�as:

    aleatorio = (rand() % 15) + 5;

    for (contador = 0; contador < aleatorio; contador++){
        for (indice_pecas = 0; indice_pecas < 56; indice_pecas += 2){

            indice_aleatorio = rand() % 55;

            if (indice_aleatorio % 2 != 0){
                indice_aleatorio++;
            }

            temp = pecas[indice_pecas];
            pecas[indice_pecas] = pecas[indice_aleatorio];
            pecas[indice_aleatorio] = temp;

            temp = pecas[indice_pecas+1];
            pecas[indice_pecas+1] = pecas[indice_aleatorio+1];
            pecas[indice_aleatorio+1] = temp;

        }
    }

    // Reordena��o de lados aleat�ria das pe�as:

    for (indice_pecas = 0; indice_pecas < 58; indice_pecas += 2){
        aleatorio = rand() % 2;

        if (aleatorio == 1){
            temp = pecas[indice_pecas];
            pecas[indice_pecas] = pecas[indice_pecas+1];
            pecas[indice_pecas+1] = temp;
        }
    }

    // Distribui��o aleat�ria das pe�as:

    for (indice_pecas = 0, indice_mao1 = 0, indice_mao2 = 0, indice_puxa = 0; indice_pecas < 56; indice_pecas += 2){

        aleatorio = rand() % 3;

        if (aleatorio == 0 && indice_puxa < 28){
            puxa[indice_puxa] = pecas[indice_pecas];
            puxa[indice_puxa+1] = pecas[indice_pecas+1];
            indice_puxa += 2;
        }
        else if (aleatorio == 1 && indice_mao1 < 14){
            mao1[indice_mao1] = pecas[indice_pecas];
            mao1[indice_mao1+1] = pecas[indice_pecas+1];
            indice_mao1 += 2;
        }
        else if (aleatorio == 2 && indice_mao2 < 14){
            mao2[indice_mao2] = pecas[indice_pecas];
            mao2[indice_mao2+1] = pecas[indice_pecas+1];
            indice_mao2 += 2;
        }
        else if (indice_mao1 < 14){
            mao1[indice_mao1] = pecas[indice_pecas];
            mao1[indice_mao1+1] = pecas[indice_pecas+1];
            indice_mao1 += 2;
        }
        else if (indice_mao2 < 14){
            mao2[indice_mao2] = pecas[indice_pecas];
            mao2[indice_mao2+1] = pecas[indice_pecas+1];
            indice_mao2 += 2;
        }else{
            puxa[indice_puxa] = pecas[indice_pecas];
            puxa[indice_puxa+1] = pecas[indice_pecas+1];
            indice_puxa += 2;
        }
    }

    // Preenchimento de indices ainda n�o utilizados com o valor inv�lido padr�o -1:

    for (; indice_mao1 < 42; indice_mao1++, indice_mao2++){
        mao1[indice_mao1] = -1;
        mao2[indice_mao2] = -1;
    }

    for (indice_tabuleiro = 0; indice_tabuleiro < 56; indice_tabuleiro++){
        tabuleiro[indice_tabuleiro] = -1;
    }

    // In�cio do c�digo que determina quem iniciar� a partida:

    if (primeira_jogada_e_partida && !jogoCarregado){

        // Verifica se h� carro�a na m�o de algum jogador:

        temCarroca1 = false;
        indice_mao1 = 0;

        while(mao1[indice_mao1] != -1 && !temCarroca1){

            if (mao1[indice_mao1] == mao1[indice_mao1+1]){
                temCarroca1 = true;
            }

            indice_mao1 += 2;
        }


        temCarroca2 = false;
        indice_mao2 = 0;

        while(mao2[indice_mao2] != -1 && !temCarroca2){

            if (mao2[indice_mao2] == mao2[indice_mao2+1]){
                temCarroca2 = true;
            }

            indice_mao2 += 2;
        }


        // Determina a maior carro�a ou a maior pe�a com maiores lados da m�o 1:

        somatorio_peca1 = 0;
        maior1_superior = -3;
        maior1_inferior = -3;

        for (indice_mao1 = 0; indice_mao1 < 14; indice_mao1 += 2){

            if (temCarroca1 && mao1[indice_mao1] == mao1[indice_mao1+1] || !temCarroca1){

                somatorio_peca1 = mao1[indice_mao1] + mao1[indice_mao1+1];
                soma_maior = maior1_superior + maior1_inferior;

                if (somatorio_peca1 > soma_maior){
                    maior1_superior =  mao1[indice_mao1];
                    maior1_inferior = mao1[indice_mao1+1];
                    localizacao_peca_maior1 = indice_mao1 / 2;
                }
                else if (somatorio_peca1 == soma_maior){
                    if (mao1[indice_mao1] > maior1_superior && mao1[indice_mao1] > maior1_inferior ||
                        mao1[indice_mao1+1] > maior1_superior && mao1[indice_mao1+1] > maior1_inferior){
                                maior1_superior =  mao1[indice_mao1];
                                maior1_inferior = mao1[indice_mao1+1];
                                localizacao_peca_maior1 = indice_mao1 / 2;
                        }
                }

            }
        }

        // Determina a maior carro�a ou a maior pe�a com maiores lados da m�o 2:

        somatorio_peca2 = 0;
        maior2_superior = -3;
        maior2_inferior = -3;

        for (indice_mao2 = 0; indice_mao2 < 14; indice_mao2 += 2){

            if (temCarroca2 && mao2[indice_mao2] == mao2[indice_mao2+1] || !temCarroca2){

                somatorio_peca2 = mao2[indice_mao2] + mao2[indice_mao2+1];
                soma_maior = maior2_superior + maior2_inferior;

                if (somatorio_peca2 > soma_maior){
                    maior2_superior = mao2[indice_mao2];
                    maior2_inferior = mao2[indice_mao2+1];
                    localizacao_peca_maior2 = indice_mao2 / 2;
                }
                else if (somatorio_peca2 == soma_maior){
                    if (mao2[indice_mao2] > maior2_superior && mao2[indice_mao2] > maior2_inferior ||
                        mao2[indice_mao2+1] > maior2_superior && mao2[indice_mao2+1] > maior2_inferior){
                            maior2_superior = mao2[indice_mao2];
                            maior2_inferior = mao2[indice_mao2+1];
                            localizacao_peca_maior2 = indice_mao2 / 2;
                        }
                }

            }
        }

        // Testa qual dos jogadores obteve a maior pe�a com maiores lados e determina quem come�ar�:

        somatorio_peca1 = maior1_superior + maior1_inferior;
        somatorio_peca2 = maior2_superior + maior2_inferior;

        if (temCarroca1 && !temCarroca2){
            vez = true;
        }
        else if (temCarroca2 && !temCarroca1){
            vez = false;
        }
        else if (somatorio_peca1 > somatorio_peca2){
            vez = true;
        }
        else if (somatorio_peca2 > somatorio_peca1){
            vez = false;
        }else{

            if (maior1_superior > maior2_superior && maior1_superior > maior2_inferior ||
                maior1_inferior > maior2_superior && maior1_inferior > maior2_inferior){
                    vez = true;
                }else{
                    vez = false;
                }

        }

    }

    else if (!jogoCarregado){
        vez = ganhou1;
    }

    // Fim do c�digo que determina quem iniciar� a partida:
}

/**
    Determina o modo do jogo de acordo com a entrada do usu�rio.
*/
void entradaModo(){
    system("cls");
    cout << "1 " << ascii(26) << " Humano contra humano" << endl;
    cout << "2 " << ascii(26) << " Humano contra computador" << endl;

    cin >> entrada_modo;

    if (entrada_modo == 1){
        humano_contra_computador = false;
    }
    else if (entrada_modo == 2){
        humano_contra_computador = true;
    }else{
        imprimeLinha();
        imprimeMensagemEntradaInvalida();
        jogadaInvalida();
        entradaModo();
    }

    // Muda o nome dos jogador 2:

    if (humano_contra_computador){
        jogador2[0] = 'C';
        jogador2[1] = 'o';
        jogador2[2] = 'm';
        jogador2[3] = 'p';
        jogador2[4] = 'u';
        jogador2[5] = 't';
        jogador2[6] = 'a';
        jogador2[7] = 'd';
        jogador2[8] = 'o';
        jogador2[9] = 'r';
        jogador2[10] = '\0';
    }else{
        jogador2[0] = 'J';
        jogador2[1] = 'o';
        jogador2[2] = 'g';
        jogador2[3] = 'a';
        jogador2[4] = 'd';
        jogador2[5] = 'o';
        jogador2[6] = 'r';
        jogador2[7] = ' ';
        jogador2[8] = '2';
        jogador2[9] = '\0';
    }
}

/**
    Determina o nome dos jogadores de acordo com as entradas do usu�rio.
    Se o jogo estiver no modo "Humano contra computador", o nome do jogador 2
    ser� definido como "Computador" automaticamente.
*/
void entradaNomeJogadores(){
    system("cls");
    cout << "Informe o nome do primeiro jogador: " << endl;

    // Corrige bug:
    cin.getline(jogador1, 1);

    // Entrada definitiva:
    cin.getline(jogador1, 51);

    if (!humano_contra_computador){
        cout << "Informe o nome do segundo jogador: " << endl;
        cin.getline(jogador2, 51);
    }else{
        cout << "O segundo jogador se chamar� \"Computador\"." << endl;
        Sleep(2000);
    }
}

/**
    Executa a op��o "jogar".
*/
void jogar(){

    if (humano_contra_computador && !vez){
        if (primeira_jogada_e_partida){
            entrada_peca_jogada = localizacao_peca_maior2;
        }else{
            entrada_peca_jogada = escolhePeca();
        }

        cout << "O computador escolheu a pe�a " << entrada_peca_jogada+1 << "." << endl;
        Sleep(tempo_com);
    }else{
        cout << "Digite o �ndice da pe�a a ser jogada: " << endl;
        cin >> entrada_peca_jogada;
        entrada_peca_jogada--;
    }

    if (vez){

        if (entrada_peca_jogada+1 > tamanho_mao1 / 2 || entrada_peca_jogada+1 <= 0){
            imprimeLinha();
            cout << "A m�o n�o cont�m uma pe�a com o �ndice informado." << endl;
            jogada_invalida = true;
            jogadaInvalida();
            return;
        }

        parte_superior = mao1[entrada_peca_jogada*2];
        parte_inferior = mao1[entrada_peca_jogada*2+1];
    }else{

        if (entrada_peca_jogada+1 > tamanho_mao2 / 2 || entrada_peca_jogada+1 <= 0){
            imprimeLinha();
            cout << "A m�o n�o cont�m uma pe�a com o �ndice informado." << endl;
            jogada_invalida = true;
            jogadaInvalida();
            return;
        }

        parte_superior = mao2[entrada_peca_jogada*2];
        parte_inferior = mao2[entrada_peca_jogada*2+1];
    }

    // Garante que a maior pe�a seja jogada caso seja a primeira partida:

    if (primeira_jogada_e_partida){

        if (vez){
            if (maior1_superior != parte_superior || maior1_inferior != parte_inferior){
                jogada_invalida = true;
                imprimeLinha();
                cout << "Voc� deve jogar a pe�a " << localizacao_peca_maior1+1 << "." << endl;
                jogadaInvalida();
                return;
            }
        }else{
            if (maior2_superior != parte_superior || maior2_inferior != parte_inferior){
                jogada_invalida = true;
                imprimeLinha();
                cout << "Voc� deve jogar a pe�a " << localizacao_peca_maior2 << "." << endl;
                jogadaInvalida();
                return;
            }
        }

    }

    // Define se � para colocar a pe�a acima ou abaixo do tabuleiro:

    colocarAcima = false;
    colocarAbaixo = false;

    if (tabuleiro[0] == -1){
        tabuleiro[0] = parte_superior;
        tabuleiro[1] = parte_inferior;
    }

    else if (laELoNaoFazDiferencaQualLado(parte_superior, parte_inferior)){
        bateu_la_e_lo = true;
        colocarAbaixo = true;
    }

    else if ( laELoFazDiferencaQualLado(parte_superior, parte_inferior) ){

            bateu_la_e_lo = true;

            if (humano_contra_computador && !vez){
                escolheLado();

                if (colocarAcima){
                    cout << "O computador escolheu colocar a pe�a acima." << endl;
                }else{
                    cout << "O computador escolheu colocar a pe�a abaixo." << endl;
                }

                Sleep(tempo_com);
            }else{

                cout << "1 - Colocar em cima." << endl;
                cout << "2 - Colocar em baixo." << endl;

                cin >> entrada_parte_tabuleiro;

                if (entrada_parte_tabuleiro == 1){
                    colocarAcima = true;
                }
                else if (entrada_parte_tabuleiro == 2){
                    colocarAbaixo = true;
                }else{
                    imprimeMensagemEntradaInvalida();
                    jogada_invalida = true;
                    jogadaInvalida();
                }

            }
    }

    else if (parte_superior == tabuleiro[0] || parte_inferior == tabuleiro[0]){
        colocarAcima = true;
    }

    else if (parte_superior == tabuleiro[tamanho_tabuleiro-1] || parte_inferior == tabuleiro[tamanho_tabuleiro-1]){
        colocarAbaixo = true;
    }

    else{
        jogada_invalida = true;
        imprimeLinha();
        cout << "Esta pe�a n�o se encaixa no tabuleiro atual." << endl;
        jogadaInvalida();
    }

    // Troca as posi��es caso seja necess�rio:
    if (colocarAcima && parte_superior == tabuleiro[0] || colocarAbaixo && parte_inferior == tabuleiro[tamanho_tabuleiro-1]){
        temp = parte_superior;
        parte_superior = parte_inferior;
        parte_inferior = temp;
    }

    // Coloca a pe�a acima ou abaixo:

    if(colocarAcima){
        for (indice_tabuleiro = tamanho_tabuleiro + 1; indice_tabuleiro >= 2; indice_tabuleiro--){
            tabuleiro[indice_tabuleiro] = tabuleiro[indice_tabuleiro-2];
        }

        tabuleiro[0] = parte_superior;
        tabuleiro[1] = parte_inferior;
    }

    else if(colocarAbaixo){
        tabuleiro[tamanho_tabuleiro] = parte_superior;
        tabuleiro[tamanho_tabuleiro+1] = parte_inferior;
    }

    // Remove a pe�a jogada da m�o do jogador da vez caso a jogada tenha sido v�lida:

    if (!jogada_invalida){

        // Incrementa o tamanho do tabuleiro:
        tamanho_tabuleiro += 2;

        if (vez){

            indice_mao1 = entrada_peca_jogada*2;

            while(mao1[indice_mao1] != -1 && indice_mao1 < 42){
               mao1[indice_mao1] = mao1[indice_mao1+2];
               indice_mao1++;
            }

            /* As �ltimas posi��es s�o preenchidas com o valor inv�lido padr�o -1 porque cont�m lixo de mem�ria
            depois do deslocamento ocorrido no �ltimo loop */

            mao1[40] = -1;
            mao2[41] = -1;

            // Decrementa o tamanho da m�o do jogador 1:
            tamanho_mao1 -= 2;
        }else{

            indice_mao2 = entrada_peca_jogada*2;

            while(mao2[indice_mao2] != -1 && indice_mao2 < 42){
               mao2[indice_mao2] = mao2[indice_mao2+2];
               indice_mao2++;
            }

            /* As �ltimas posi��es s�o preenchidas com o valor inv�lido padr�o -1 porque cont�m lixo de mem�ria
            depois do deslocamento ocorrido no �ltimo loop */

            mao1[40] = -1;
            mao2[41] = -1;

            // Decrementa o tamanho da m�o do jogador 2:
            tamanho_mao2 -= 2;
        }

    }

}

/**
    Executa a op��o "passar".
*/
void passar(){

    if (tabuleiro[0] == -1){
        imprimeLinha();
        cout << "Voc� n�o pode passar na primeira jogada." << endl;
        jogada_invalida = true;
        jogadaInvalida();
    }
    else if (vez && maoContemPecaJogavel(mao1) || !vez && maoContemPecaJogavel(mao2)){
        imprimeLinha();
        cout << "Voc� n�o pode passar porque voc� possui pe�as jog�veis." << endl;
        jogada_invalida = true;
        jogadaInvalida();
    }
    else if (puxa[0] != -1){
        imprimeLinha();
        cout << "Voc� n�o pode passar enquanto houverem pe�as no puxa." << endl;
        jogada_invalida = true;
        jogadaInvalida();
    }

}

/**
    Executa a op��o "puxar".
*/
void puxar(){

    if (puxa[0] == -1){
        imprimeLinha();
        cout << "O puxa acabou. Use outra op��o." << endl;
        jogadaInvalida();
        jogada_invalida = true;
    }

    else if(tabuleiro[0] == -1){
        jogada_invalida = true;
        imprimeLinha();
        cout << "Voc� n�o pode puxar na primeira jogada." << endl;
        jogadaInvalida();
    }

    else if (vez && maoContemPecaJogavel(mao1) || !vez && maoContemPecaJogavel(mao2)){
        jogada_invalida = true;
        imprimeLinha;
        cout << "Voc� n�o pode puxar porque voc� possui pe�as jog�veis." << endl;
        jogadaInvalida();
    }

    else{

        if(vez){
            mao1[tamanho_mao1] = puxa[tamanho_puxa-2];
            mao1[tamanho_mao1+1] = puxa[tamanho_puxa-1];
            // Incrementa o tamanho da m�o do jogador 1:
            tamanho_mao1 += 2;
        }else{
            mao2[tamanho_mao2] = puxa[tamanho_puxa-2];
            mao2[tamanho_mao2+1] = puxa[tamanho_puxa-1];
            // Incrementa o tamanho da m�o do jogador 2:
            tamanho_mao2 += 2;
        }

        puxa[tamanho_puxa-2] = -1;
        puxa[tamanho_puxa-1] = -1;
        puxou = true;

        // Decrementa o tamanho do puxa:
        tamanho_puxa -= 2;

    }

}

/**
    Executa o c�digo referente a uma partida.
*/
void partida(){

    // Inicializa��o das vari�veis a serem utilizadas dentro do loop que define uma partida:

    puxou = false;
    tamanho_tabuleiro = 0;
    tamanho_mao1 = 14;
    tamanho_mao2 = 14;
    tamanho_puxa = 28;
    bateu_carroca = false;
    bateu_la_e_lo = false;

    determinacoesIniciais();

    ganhou1 = false;
    ganhou2 = false;

    // Inicio do loop que define uma partida:

    while(!ganhou1 && !ganhou2){

        // Define a cor do prompt

        if (vez){
            system("color 2");
        }else{
            system("color 6");
        }

        system("cls");

        // In�cio da impress�o do tabuleiro:

        if (tabuleiro[0] != -1){
            cout << "Tabuleiro: " << endl;
            imprimeTabuleiro();
        }

        // Fim da impress�o do tabuleiro.

        // Imprime o nome do jogador da primeira partida e jogada:

        if (primeira_jogada_e_partida){

            if (vez){
                imprimeJogadorDaPrimeiraPartida(jogador1, localizacao_peca_maior1);
            }else{
                imprimeJogadorDaPrimeiraPartida(jogador2, localizacao_peca_maior2);
            }

            PausarOuDormir();
        }

        // Imprime o nome do jogador da vez caso n�o tenha havido puxe na �ltima jogada:

        else if (!puxou){

            if (vez){
                imprimeJogadoDaVez(jogador1);
            }else{
                imprimeJogadoDaVez(jogador2);
            }

            PausarOuDormir();

        }else{
            puxou = false;
        }

        if (humano_contra_computador && !vez){
            cout << "M�o do computador: " << endl;
        }else{
            cout << "Sua m�o: " << endl;
        }

        // Imprime a m�o com indices num�ricos:

        imprimeLinha();

        if (vez){
            imprimeMao(mao1);
        }else{
            imprimeMao(mao2);
        }

        // Dorme case seja a vez do computador:

        imprimeLinha();

        if (humano_contra_computador && !vez){
            Sleep(tempo_com);
        }

        // Imprime o status do puxa no canto direito:

        if (puxa[0] == -1){
            imprimeEspacos(61);
            cout << "O puxa est� vazio." << endl;
        }else{
            imprimeEspacos(54);
            cout << "O puxa cont�m " << tamanho_puxa / 2 << " pe�a(s)." << endl;
        }

        // Menu de op��es que se repete em caso de jogada inv�lida:

        do{
            jogada_invalida = false;

            // Menu de op��es:

            imprimeLinha();
            cout << "(a) " << ascii(26) << " Jogar." << endl;
            cout << "(b) " << ascii(26) << " Passar." << endl;
            cout << "(c) " << ascii(26) << " Puxar." << endl;
            imprimeLinha();

            if (humano_contra_computador && !vez){

                Sleep(tempo_com);

                if (maoContemPecaJogavel(mao2)|| tabuleiro[0] == -1){
                    entrada_menu_opcoes = 'a';
                    cout << "O computador escolheu a op��o (a)." << endl;
                    tempo_com = 1350;
                    Sleep(tempo_com);
                }
                else if (puxa[0] == -1){
                    entrada_menu_opcoes = 'b';
                    cout << "O computador escolheu a op��o (b)." << endl;
                    Sleep(tempo_com);
                }else{
                    entrada_menu_opcoes = 'c';
                    cout << "O computador escolheu a op��o (c)." << endl;
                    tempo_com = 900;
                    Sleep(tempo_com);
                }

            }else{
                cin >> entrada_menu_opcoes;
            }
            // Converte a letra informada para min�scula caso ela seja mai�scula:

            entrada_menu_opcoes = tolower(entrada_menu_opcoes);

            // Testa as op��es:

            if (entrada_menu_opcoes == 'a'){
                jogar();
            }
            else if (entrada_menu_opcoes == 'b'){
               passar();
            }

            else if (entrada_menu_opcoes == 'c'){
                puxar();
            }
            else{
                jogada_invalida = true;
                imprimeMensagemEntradaInvalida();
                jogadaInvalida();
            }

        }while(jogada_invalida);

        // Testa se algu�m ganhou esvaziando a m�o:

        if (mao1[0] == -1){
            ganhou1 = true;
        }
        else if (mao2[0] == -1){
            ganhou2 = true;
        }

        // Testa se ganhou com carroca e/ou la e lo:

        if (ganhou1 || ganhou2){
            bateu_carroca = (parte_superior == parte_inferior);
        }else{
            bateu_la_e_lo = false;
        }

        // Verifica se o jogo est� travado caso o puxa tiver acabado e houverem pecas no tabuleiro:

        jogoTravado = false;

        if (puxa[0] == -1 && tabuleiro[0] != -1 && !ganhou1 && !ganhou2){
            jogoTravado = !( maoContemPecaJogavel(mao1) || maoContemPecaJogavel(mao2) );
        }

        // Verifica quem ganhou caso o jogo esteja travado

        if (jogoTravado){

            indice_mao1 = 0;
            indice_mao2 = 0;
            soma_pecas1 = 0;
            soma_pecas2 = 0;

            while(mao1[indice_mao1] != -1){
                soma_pecas1 += mao1[indice_mao1];
                indice_mao1++;
            }

            while(mao2[indice_mao2] != -1){
                soma_pecas2 += mao2[indice_mao2];
                indice_mao2++;
            }

            if (soma_pecas1 < soma_pecas2){
                ganhou1 = true;
            }
            else if (soma_pecas1 > soma_pecas2){
                ganhou2 = true;
            }else{
                if (vez){
                    ganhou2 = true;
                }else{
                    ganhou1 = true;
                }
            }
        }

        // Determina que j� n�o � mais a primeira jogada da primeira partida:
        if (primeira_jogada_e_partida){
            primeira_jogada_e_partida = false;
        }

        // Passa a vez caso a �ltima jogada n�o tenha sido um puxe:

        if (!puxou && !ganhou1 && !ganhou2){
            vez = !vez;
        }
    }

    system("cls");

    // In�cio da impress�o da montagem final do tabuleiro:

    cout << "Tabuleiro Final: " << endl;

    imprimeTabuleiro();

    // Fim da impress�o da montagem final do tabuleiro.

    if (ganhou1){
        imprimeMensagemVitoriaESomaPontos(jogador1);
    }else{
        imprimeMensagemVitoriaESomaPontos(jogador2);
    }

    system("pause");

}

/**
    M�todo principal.
*/
int main(){

    semente = time(0);
    srand(semente);
    primeira_jogada_e_partida = true;
    pontuacao1 = 0;
    pontuacao2 = 0;
    humano_contra_computador = false;
    repetir_menu_inicial = true;
    jogoCarregado = false;
    tempo_com = 1350;

    imprimeArteAscii();
    naoUsareiAscii();

    // Gera��o de todas as pe�as:

    for (indice_pecas = 0, cima = 0, baixo = 0; indice_pecas < 56; indice_pecas++){
        if (indice_pecas % 2 == 0){
            pecas[indice_pecas] = cima;
        }else{
            pecas[indice_pecas] = baixo;
            baixo++;
        }

        if (baixo == 7){
            cima++;
            baixo = cima;
        }
    }

    // Menu de op��es iniciais:

    do{
        system("color 7");
        system("cls");

        imprimeLinha();
        cout << "(a) " << ascii(26) << " Iniciar novo jogo" << endl;
        cout << "(b) " << ascii(26) << " Mudar nome dos jogadores" << endl;
        cout << "(c) " << ascii(26) << " Mudar modo de jogo" << endl;
        cout << "(d) " << ascii(26) << " Carregar jogo" << endl;
        cout << "(e) " << ascii(26) << " Sair" << endl;
        imprimeLinha();

        if (jogoCarregado){
            entrada_menu_inicial_opcoes = 'a';
        }else{
            cin >> entrada_menu_inicial_opcoes;
        }

        entrada_menu_inicial_opcoes = tolower(entrada_menu_inicial_opcoes);

        if (entrada_menu_inicial_opcoes == 'a'){

            continuar_jogo = true;

            while(continuar_jogo){  // In�cio do jogo.
                partida();
                continuar_jogo = (pontuacao1 < 6 && pontuacao2 < 6);

                if (continuar_jogo){

                    do{
                        system("cls");
                        jogada_invalida = false;

                        cout << "(a) " << ascii(26) << " Continuar jogo" << endl;
                        cout << "(b) " << ascii(26) << " Salvar jogo" << endl;
                        cin >> entrada_termino_partida;

                        entrada_termino_partida = tolower(entrada_termino_partida);

                        if (entrada_termino_partida == 'a'){
                            // N�o executo nada. O programa vai para a pr�xima partida.
                        }
                        else if (entrada_termino_partida == 'b'){
                            salvaJogo();
                        }else{
                            imprimeMensagemEntradaInvalida();
                            jogadaInvalida();
                            jogada_invalida = true;
                        }

                    }while(jogada_invalida);

                }

            } // Fim do jogo.

            // Reinicializa��o de vari�veis:
            jogoCarregado = false;
            primeira_jogada_e_partida = true;

            imprimeLinha();

            if (pontuacao1 >= 6){
                cout << jogador1 << " � o vencedor do jogo!" << endl;
            }else{
                cout << jogador2 << " � o vencedor do jogo!" << endl;
            }

            imprimeLinha();

            cout << '\a';

            system("pause");

        }
        else if (entrada_menu_inicial_opcoes == 'b'){
            entradaNomeJogadores();
        }

        else if (entrada_menu_inicial_opcoes == 'c'){
            entradaModo();
        }

        else if (entrada_menu_inicial_opcoes == 'd'){
            carregaJogo();
        }

        else if (entrada_menu_inicial_opcoes == 'e'){
            system("cls");
            cout << "O jogo est� encerrando..." << endl;
            repetir_menu_inicial = false;
        }

        else{
            imprimeMensagemEntradaInvalida();
            jogadaInvalida();
        }

    }while(repetir_menu_inicial);

    return 0;
}
